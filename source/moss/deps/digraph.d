/*
 * SPDX-FileCopyrightText: Copyright © 2020-2022 Serpent OS Developers
 *
 * SPDX-License-Identifier: Zlib
 */

/**
 * moss.deps.digraph
 *
 * Define a directed graph data structure with vertices for use in dependency
 * resolution.
 *
 * Authors: Copyright © 2020-2022 Serpent OS Developers
 * License: Zlib
 */
module moss.deps.digraph;

import std.container.rbtree;
import std.exception : enforce;
import std.string : format;
import std.stdio : File, stdout;

/**
 * Validate **basic** topological sorting. We achieve this by passing a closure
 * to the depth-first search functions.
 */
unittest
{
    static struct Pkg
    {
        string name;
        string[] dependencies;
    }

    Pkg[] pkgs = [
        Pkg("baselayout"), Pkg("glibc", ["baselayout"]),
        Pkg("nano", ["libtinfo", "ncurses", "glibc"]), Pkg("libtinfo",
                ["glibc"]), Pkg("ncurses", ["libtinfo", "glibc"]),
    ];
    auto g = new DirectedAcyclicalGraph!string();
    foreach (p; pkgs)
    {
        g.addVertex(p.name);
        foreach (d; p.dependencies)
        {
            g.addEdge(p.name, d);
        }
    }
    immutable auto expectedOrder = [
        "baselayout", "glibc", "libtinfo", "ncurses", "nano"
    ];
    string[] computedOrder;
    g.topologicalSort((n) { computedOrder ~= n; });
    assert(computedOrder == expectedOrder, "Wrong ordering of dependencies");

    g.emitGraph();
}

/**
 * Track status of vertex visits
 */
private enum VertexStatus
{
    /**
     * Not yet discovered
     */
    Undiscovered = 0,

    /**
     * Currently within the vertex
     */
    Discovered,

    /**
     * We're done with this vertex
     */
    Explored,
}

/**
 * We use allocated Vertex structs to maintain each vertex, or node, in our
 * graph structure. Additionally each Vertex may contain a set of edges that
 * connect it to another Vertex in a single direction.
 */
private struct Vertex(L)
{
    alias LabelType = L;
    alias EdgeStorage = RedBlackTree!(LabelType, "a < b", false);

    /**
     * Label is used for sorting the vertices and referencing it
     */
    immutable(LabelType) label;

    /**
     * Store any edge references
     */
    EdgeStorage edges;

    /**
     * Factory to create a new Vertex
     */
    static Vertex!(LabelType)* create(in LabelType label)
    {
        return new Vertex!(LabelType)(cast(immutable(LabelType)) label, new EdgeStorage());
    }

    /**
     * Factory to create a comparator Vertex
     */
    static Vertex!(LabelType) refConstruct(in LabelType label)
    {
        return Vertex!(LabelType)(cast(immutable(LabelType)) label);
    }

    /**
     * Return true if both vertices are equal
     */
    bool opEquals()(auto ref const Vertex!(LabelType) other) const
    {
        return other.label == this.label;
    }

    /**
     * Compare two vertices with the same type
     */
    int opCmp(ref const Vertex!(LabelType) other) const
    {
        if (this.label < other.label)
        {
            return -1;
        }
        else if (this.label > other.label)
        {
            return 1;
        }
        return 0;
    }

    /**
     * Return the hash code for the label
     */
    ulong toHash() @safe nothrow const
    {
        return typeid(LabelType).getHash(&label);
    }

    /**
     * Visitation status of the node
     */
    VertexStatus status = VertexStatus.Undiscovered;
}

/**
 * The Directed Acyclical Graoh is used for ordering information and ensuring completeness, whilst
 * detecting dependency cycles.
 *
 * The use of DependencyGraph will be expanded upon in future to permit more
 * intelligent use than a simple Depth-First Search, so that we can support
 * multiple candidate scenarios.
 */
public final class DirectedAcyclicalGraph(L)
{
    alias LabelType = L;
    alias VertexDescriptor = Vertex!(LabelType);
    alias VertexTree = RedBlackTree!(VertexDescriptor*, "a.label < b.label", false);
    alias DfsClosure = void delegate(LabelType l);

    /**
     * Construct a new DependencyGraph
     */
    this()
    {
        vertices = new VertexTree();
    }

    /**
     * Return true if we already have this node
     */
    bool hasVertex(in LabelType label)
    {
        auto desc = VertexDescriptor.refConstruct(label);
        return !vertices.equalRange(&desc).empty;
    }

    /**
     * Add a new node to the tree.
     */
    void addVertex(in LabelType label)
    {
        enforce(!hasVertex(label), "Cannot add duplicate node");
        vertices.insert(VertexDescriptor.create(label));
    }

    /**
     * Add an edge between the two named vertices
     */
    void addEdge(in LabelType u, in LabelType v)
    {
        auto desc = VertexDescriptor.refConstruct(u);
        auto match = vertices.equalRange(&desc);
        enforce(!match.empty, "Cannot find node: %s".format(u));

        match.front.edges.insert(cast(LabelType) v);

    }

    /**
     * Perform depth first search and execute closure on encountered nodes
     */
    void topologicalSort(DfsClosure cb)
    {
        enforce(cb !is null, "Cannot perform search without a closure");
        /* Discolour/reset each vertex */
        foreach (vertex; vertices)
        {
            vertex.status = VertexStatus.Undiscovered;
        }

        /* For every unvisited vertex, begin search */
        foreach (vertex; vertices)
        {
            if (vertex.status == VertexStatus.Undiscovered)
            {
                dfsVisit(vertex, cb);
            }
        }
    }

    /**
     * Emit the graph to the given output stream.
     * Highly simplistic
     */
    void emitGraph(File output = stdout)
    {
        import std.conv : to;

        output.writeln("digraph G {");
        foreach (v; vertices)
        {
            if (v.edges.empty)
            {
                output.writefln!"%s;"(v.label.to!string);
                continue;
            }
            foreach (edge; v.edges)
            {
                output.writefln!"%s -> %s;"(v.label.to!string, getVertex(edge).label.to!string);
            }
        }
        output.writeln("}");
    }

    /**
     * Automatically break U->V->U cycle dependencies
     */
    void breakCycles()
    {
        foreach (v; vertices)
        {
            restart: foreach (edge; v.edges)
            {
                auto lookupNode = getVertex(edge);
                auto matches = lookupNode.edges.equalRange(cast(LabelType) v.label);
                if (!matches.empty)
                {
                    lookupNode.edges.removeKey(matches);
                    goto restart;
                }
            }
        }
    }

    /**
     * Returns a new DAG that is a transposed version of this one.
     */
    DirectedAcyclicalGraph!LabelType reversed()
    {
        auto ret = new DirectedAcyclicalGraph!LabelType();

        foreach (vertex; vertices)
        {
            auto u = vertex.label;
            if (!ret.hasVertex(u))
            {
                ret.addVertex(u);
            }
            foreach (v; vertex.edges)
            {
                if (!ret.hasVertex(v))
                {
                    ret.addVertex(v);
                }
                /* Swap V with U */
                ret.addEdge(v, u);
            }
        }

        return ret;
    }

    /**
     * Return a new graph that is a subgraph starting with only the rootVertex.
     */
    DirectedAcyclicalGraph!LabelType subgraph(in LabelType rootVertex)
    {
        auto match = getVertex(rootVertex);
        auto dag = new DirectedAcyclicalGraph!LabelType();

        void addClone(VertexDescriptor* vd)
        {
            if (!dag.hasVertex(vd.label))
            {
                dag.addVertex(vd.label);
            }
            foreach (edge; vd.edges)
            {
                addClone(getVertex(edge));
                dag.addEdge(vd.label, edge);
            }
        }

        addClone(match);

        return dag;
    }

private:

    /**
     * Helper to return a node
     */
    auto getVertex(in LabelType v)
    {
        auto desc = VertexDescriptor.refConstruct(v);
        auto match = vertices.equalRange(&desc);
        enforce(!match.empty, "Cannot find node: %s".format(v));

        return match.front;
    }

    /**
     * Internal depth first search visit logic
     */
    void dfsVisit(VertexDescriptor* vertex, DfsClosure cb)
    {
        vertex.status = VertexStatus.Discovered;
        foreach (edge; vertex.edges)
        {
            auto edgeNode = getVertex(edge);

            /* Not yet visited, go take a looksie */
            if (edgeNode.status == VertexStatus.Undiscovered)
            {
                dfsVisit(edgeNode, cb);
            }
            /* Dun dun dun, cycle. */
            else if (edgeNode.status == VertexStatus.Discovered)
            {
                throw new Exception("Encountered dependency cycle between %s and %s".format(edgeNode.label,
                        vertex.label));
            }
        }

        /* Done, yield the result */
        vertex.status = VertexStatus.Explored;
        cb(cast(LabelType) vertex.label);
    }

    VertexTree vertices;
}
